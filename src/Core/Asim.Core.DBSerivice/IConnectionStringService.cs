﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Core.DBSerivice
{
    public interface IConnectionStringService
    {
        Task<string> GetConnectionString(string code, string tableVaultName, string connectionStrName, string passPhrase);
    }
}
