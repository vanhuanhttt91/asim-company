﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Core.DataProvider.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _dbContext;

        public EfRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public virtual IQueryable<T> Table => _dbContext.Set<T>();

        public virtual T GetById(object id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public virtual async Task<T> GetByIdAsync(object id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public IEnumerable<T> GetAll()
        {
            return Table.ToList();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Table.ToListAsync();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _dbContext.Set<T>().Where(expression);
        }

        public IQueryable<T> Find(ISpecification<T> specification = null)
        {
            return ApplySpecification(specification, false);
        }

        public IQueryable<T> FindSkipPaging(ISpecification<T> specification = null)
        {
            return ApplySpecification(specification, true);
        }

        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> expression)
        {
            return await _dbContext.Set<T>().Where(expression).AsNoTracking().ToListAsync();
        }
        public async Task<IEnumerable<TResult>> FindAsync<TResult>(Expression<Func<T, bool>> expression, Expression<Func<T, TResult>> selector)
        {
            return await _dbContext.Set<T>().Where(expression).AsNoTracking().Select(selector).ToArrayAsync();
        }

        public T Insert(T entity)
        {
            _dbContext.Set<T>().Add(entity);

            _dbContext.SaveChanges();

            return entity;
        }

        public async Task<T> InsertAsync(T entity)
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public int InsertRange(IEnumerable<T> entities)
        {
            _dbContext.Set<T>().AddRange(entities);

            return _dbContext.SaveChanges();
        }

        public async Task<int> InsertRangeAsync(IEnumerable<T> entities)
        {
            await _dbContext.Set<T>().AddRangeAsync(entities);

            return await _dbContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<T>> InsertMultiRecordsAsync(IEnumerable<T> entities)
        {
            await _dbContext.Set<T>().AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
            return entities;
        }

        public T Update(T entity)
        {
            _dbContext.Set<T>().Update(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            _dbContext.Set<T>().Update(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<int> UpdateRangeAsync(IEnumerable<T> entities)
        {
            _dbContext.Set<T>().UpdateRange(entities);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> DeleteRangeAsync(IEnumerable<T> entities)
        {
            _dbContext.Set<T>().RemoveRange(entities);
            return await _dbContext.SaveChangesAsync();
        }

        public int Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            return _dbContext.SaveChanges();
        }

        public int Delete(Expression<Func<T, bool>> expression)
        {
            var entities = _dbContext.Set<T>().Where(expression);
            _dbContext.Set<T>().RemoveRange(entities);

            return _dbContext.SaveChanges();
        }

        public async Task<int> DeleteAsync(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(Expression<Func<T, bool>> expression)
        {
            var entities = _dbContext.Set<T>().Where(expression);
            _dbContext.Set<T>().RemoveRange(entities);
            return await _dbContext.SaveChangesAsync();
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec, bool skipPaging)
        {
            return SpecificationEvaluator<T>.GetQuery(_dbContext.Set<T>().AsQueryable(), spec, skipPaging);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return await Table.FirstOrDefaultAsync(expression);
        }

        public void Clear()
        {
            _dbContext.ChangeTracker.Clear();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> expression)
        {
            return await Table.AnyAsync(expression);
        }
    }
}