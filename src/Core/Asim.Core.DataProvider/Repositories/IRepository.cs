﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Core.DataProvider.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Table { get; }

        T GetById(object id);

        Task<T> GetByIdAsync(object id);

        IEnumerable<T> GetAll();

        Task<IEnumerable<T>> GetAllAsync();

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression);

        IEnumerable<T> Find(Expression<Func<T, bool>> expression);

        IQueryable<T> Find(ISpecification<T> specification = null);

        IQueryable<T> FindSkipPaging(ISpecification<T> specification = null);

        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> expression);
        Task<IEnumerable<TResult>> FindAsync<TResult>(Expression<Func<T, bool>> expression, Expression<Func<T, TResult>> selector);
        T Insert(T entity);

        Task<T> InsertAsync(T entity);

        int InsertRange(IEnumerable<T> entities);

        Task<int> InsertRangeAsync(IEnumerable<T> entities);
        Task<IEnumerable<T>> InsertMultiRecordsAsync(IEnumerable<T> entities);

        T Update(T entity);

        Task<T> UpdateAsync(T entity);

        Task<int> UpdateRangeAsync(IEnumerable<T> entities);

        Task<int> DeleteRangeAsync(IEnumerable<T> entities);

        int Delete(T entity);

        int Delete(Expression<Func<T, bool>> expression);

        Task<int> DeleteAsync(T entity);

        Task<int> DeleteAsync(Expression<Func<T, bool>> expression);
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        void Clear();
    }
}