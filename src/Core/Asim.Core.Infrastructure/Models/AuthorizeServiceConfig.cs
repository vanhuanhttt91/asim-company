﻿namespace Asim.Core.Infrastructure.Models
{
    public class AuthorizeServiceConfig
    {
        public string EndPoint { get; set; }
    }
}
