﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Core.Infrastructure.Models
{
    public class AppConfiguration
    {
        public AuthorizeServiceConfig AuthorizeServiceConfig { get; set; }
    }
}
