﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace Asim.Core.Infrastructure.MvcCustomizations
{
    public class NotFoundResultApiConvention : ApiConventionBase
    {
        protected override void ApplyControllerConvention(ControllerModel controller)
        {
            controller.Filters.Add(new NotFoundResultAttribute());
        }
    }
}
