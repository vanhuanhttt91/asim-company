﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Asim.Core.Infrastructure.MvcCustomizations
{
    internal class NotFoundResultAttribute : IFilterMetadata
    {
    }
}