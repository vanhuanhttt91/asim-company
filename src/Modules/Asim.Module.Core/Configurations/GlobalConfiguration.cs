﻿using System.Collections.Generic;

namespace Asim.Module.Core.Configurations
{
    public static class GlobalConfiguration
    {
        public static IList<ModuleInfo> Modules { get; set; } = new List<ModuleInfo>();
    }
}
