﻿using Asim.Core.Infrastructure.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Module.Core.Configurations
{
    public static class ConfigureServiceModulesExtension
    {
        public static IServiceCollection RegisterModules(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions().Configure<AppConfiguration>(configuration);
            //services.AddHttpClient<IAuthenticationService, AuthenticationService>(c =>
            //{
            //    string endPoint = configuration["Endpoints:AuthorizeApi"];
            //    c.BaseAddress = new System.Uri(endPoint);
            //    c.DefaultRequestHeaders.Add("Accept", "application/json");
            //});
            //services.AddHttpClient<IHttpRequestFactory, HttpRequestFactory>(c =>
            //{
            //    c.DefaultRequestHeaders.Add("Accept", "application/json");
            //});

        
            //services.AddTransient<ILoggerService, LoggerService>();
            

            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            foreach (var dll in Directory.GetFiles(path, "Asim.*"))
            {
                if (dll.EndsWith(".dll"))
                {
                    var assemblyName = AssemblyName.GetAssemblyName(dll);
                    var moduleInfo = new ModuleInfo
                    {
                        Assembly = Assembly.Load(assemblyName)
                    };
                    GlobalConfiguration.Modules.Add(moduleInfo);
                }
            }
            foreach (var module in GlobalConfiguration.Modules)
            {
                var moduleInitializerType = module.Assembly.GetTypes()
                   .FirstOrDefault(t => typeof(IModuleInitializer).IsAssignableFrom(t));
                if ((moduleInitializerType != null) && (moduleInitializerType != typeof(IModuleInitializer)))
                {
                    var moduleInitializer = (IModuleInitializer)Activator.CreateInstance(moduleInitializerType);

                    moduleInitializer.ConfigureServices(services, configuration);
                }
            }
            return services;
        }
    }
}