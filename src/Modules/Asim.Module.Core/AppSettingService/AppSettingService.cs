﻿using Asim.Module.Core.AppSettingService.Models;
using Microsoft.Extensions.Options;

namespace Asim.Module.Core.AppSettingService
{
    public class AppSettingService : IAppSettingService
    {
        private readonly AppSettings appSettings;

        public AppSettingService(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
        }

        public VaultInfo VaultInfo => appSettings.VaultInfo;

        public Endpoints Endpoints => appSettings.Endpoints;
    }
}
