﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Module.Core.AppSettingService.Models
{
    public class VaultInfo
    {
        public string Code { get; set; }
        public string VaultTable { get; set; }
        public string VaultPasswordPassphrase { get; set; }
        public string ConnectionStringName { get; set; }
    }
}
