﻿namespace Asim.Module.Core.AppSettingService.Models
{
    public class AppSettings
    {
        public VaultInfo VaultInfo { get; set; }
        public Endpoints Endpoints { get; set; }
    }
}
