﻿using Asim.Module.Core.AppSettingService.Models;

namespace Asim.Module.Core.AppSettingService
{
    public interface IAppSettingService
    {
        VaultInfo VaultInfo { get; }

        Endpoints Endpoints { get; }

        //AwsCollection AwsCollection { get; }

        //EmailSettings EmailSettings { get; }
    }
}
