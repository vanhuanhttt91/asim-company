﻿using Asim.Core.Infrastructure.Controllers;
using Asim.Module.Product.Bussiness.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Asim.Module.Product.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : BaseApiController
    {
        private readonly IProductBussiness _productBussiness;
        public ProductController(IProductBussiness productBussiness) {
            _productBussiness = productBussiness;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var product = await _productBussiness.GetProductAsync(id);
            return Ok(product);
        }
    }
}
