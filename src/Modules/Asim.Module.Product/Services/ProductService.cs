﻿using Asim.Module.Product.DTOs;
using Asim.Module.Product.Repositories;
using System.Threading.Tasks;
using ProductModel = Asim.Module.Product.Contexts.Models.Product;

namespace Asim.Module.Product.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository<ProductModel> _productRepository;
        public ProductService(IProductRepository<ProductModel> productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task<ProductDto> GetProductAsync(int id)
        {
            return await _productRepository.GetProductAsync(id);
        }
    }
}
