﻿using Asim.Module.Product.DTOs;
using System.Threading.Tasks;

namespace Asim.Module.Product.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetProductAsync(int id);
    }
}
