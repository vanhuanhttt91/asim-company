﻿using Asim.Module.Product.Bussiness.Interfaces;
using Asim.Module.Product.DTOs;
using Asim.Module.Product.Services;
using System.Threading.Tasks;

namespace Asim.Module.Product.Bussiness.Implements
{
    public class ProductBussiness : IProductBussiness
    {
        private readonly IProductService _productService;
        public ProductBussiness(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<ProductDto> GetProductAsync(int id)
        {
            return await _productService.GetProductAsync(id);
        }
    }
}
