﻿using Asim.Module.Product.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asim.Module.Product.Bussiness.Interfaces
{
   public interface IProductBussiness
    {
        Task<ProductDto> GetProductAsync(int id);
    }
}
