﻿using Asim.Core.DBSerivice;
using Asim.Module.Core.AppSettingService;
using Asim.Module.Core.Configurations;
using Asim.Module.Product.Contexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Microsoft.EntityFrameworkCore;
using Asim.Module.Product.Repositories;
using Asim.Module.Product.Services;
using Asim.Module.Product.Bussiness.Interfaces;
using Asim.Module.Product.Bussiness.Implements;
using Asim.Core.DBService;

namespace Asim.Module.Product
{
    class ModuleInitializer : IModuleInitializer
    {
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            throw new NotImplementedException();
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            var serviceProvider = services.BuildServiceProvider();
            var appSettingService = serviceProvider.GetRequiredService<IAppSettingService>();
            var vaultInfo = appSettingService.VaultInfo;

            string connection = vaultInfo.ConnectionStringName;
            services.AddDbContextPool<ProductDbContext>(options =>
            {
                options.UseSqlServer(connection,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                    });
            });
            ConfigIOCRepositories(services);
            ConfigIOCServices(services);
            ConfigIOCBussiness(services);
        }
        private void ConfigIOCRepositories(IServiceCollection services)
        {
            services.AddTransient(typeof(IProductRepository<>), typeof(ProductRepository<>));
        }

        private void ConfigIOCServices(IServiceCollection services)
        {
            services.AddTransient<IProductService, ProductService>();
        }
        private void ConfigIOCBussiness(IServiceCollection services)
        {
            services.AddTransient<IProductBussiness, ProductBussiness>();
        }
    }
}
