﻿namespace Asim.Module.Product.Contexts.Models
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
