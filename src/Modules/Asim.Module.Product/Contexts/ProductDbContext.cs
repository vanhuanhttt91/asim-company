﻿using Microsoft.EntityFrameworkCore;

namespace Asim.Module.Product.Contexts
{
    public partial class ProductDbContext : DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.Product>(entity =>
            {
                entity.ToTable("Product");
                entity.HasKey(x => x.Id)
                .HasName("PK_Product_Id");
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
