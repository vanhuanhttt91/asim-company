﻿using Asim.Core.DataProvider.Repositories;
using ProductModel = Asim.Module.Product.Contexts.Models.Product;
using Asim.Module.Product.DTOs;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Asim.Module.Product.Contexts;
using System.Linq;

namespace Asim.Module.Product.Repositories
{
    public class ProductRepository<T> : EfRepository<T>, IProductRepository<T> where T : class
    {
        public ProductRepository(ProductDbContext context) : base(context)
        {

        }

        public async Task<ProductDto> GetProductAsync(int id)
        {
            return await (from product in _dbContext.Set<ProductModel>()
                          where product.Id == id
                          select new ProductDto
                          {
                              Id = product.Id,
                              Name = product.Name
                          }).FirstOrDefaultAsync();
        }
    }
}
