﻿using Asim.Core.DataProvider.Repositories;
using Asim.Module.Product.DTOs;
using System.Threading.Tasks;

namespace Asim.Module.Product.Repositories
{
    public interface IProductRepository<T> : IRepository<T> where T : class
    {
        Task<ProductDto> GetProductAsync(int id);
    }
}
